########
# Level 2: Novice
#
# Time to learn some simple spells called cantrips. Once you know them you can
# advance to the next level.
#
########

# To increase the power of ones spells we need to transmute numbers into a
# number that is one higher than we had before.
def inc(n):
    """Returns a number that is 1 higher than n.

    Example: inc(3) should return 4

    """
    return n+1

# There exists both white magic that comes form the positive plane, and black
# magic that exists in the negative plane. A wizard must learn to sense which
# is which. This spell identifies white magic.
def is_positive(n):
    """Returns True if n is positive, False otherwise."""
    if n > 0:
        return True
    else: return False
# This spell identifies black magic.
def is_negative(n):
    """Returns True if n id negative, False otherwise."""
    if n < 0:
        return True
    else:
        return False
# Casting spells often requires repeating mystic words three times.
def echo(message):
    """Returns the message three times with commas and a space in between.

    Example: echo("zoot") should return "zoot, zoot, zoot".

    """
    eko = message+", "+message+", "+message
    return eko
# Wizards need to inverse the effect of some spells, turning night into day and
# black into white. This spell turns truth into lies and vice versa.
def negate(v):
    """Returns the inverse of the boolean value of v.

    Example: negate(True) should return False
    Example: negate(False) should return True

    """
    return not v
# If every wizard must have as many hats as there are wizards living in his
# tower, how many hats are there in the tower?
def square(n):
    """Returns the square of the argument n.

    Example: square(5) should return 25

    """
    return n*n
# When summoning demons, a wizard must draw a magic circle of protection large
# enough to contain the demon. This spell calculates how big such a circle
# would be.
def calculate_circle_area(radius):
    """Returns the area of a circle with the given radius.

    Pi = 3.14

    Example: calculate_circle_area(2) should return 12.56

    """
    return radius*radius*3.14
# The templars of Aiur use a special phrase to greet each other.
def greet(name):
    """Returns a greeting message saying En Taro Adun, name!.

    Example: greet("Hazimel") should return 'En Taro Adun, Hazimel!'

    """
    greeting = "En Taro Adun, " + name + "!"
    return greeting
# Every spell caster of note has an epithet ending in an exclamation mark. Add
# "...the magnificent!" to the name passed as an argument and return the result.
def wizardize_name(name):
    """Suffix a name with "...the magnificent!"

    Example: wizardize_name("Magnus") should return "Magnus...the magnificent!"

    Arguments:
    name     A string

    HINT291: UmVtZW1iZXIgdGhlICsgb3BlcmF0b3I/
   
    """
    wizardized = name + "...the magnificent!"
    return wizardized
