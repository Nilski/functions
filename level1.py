########
# Level 1: Neopythe
#
# You have just begun your journey.... practice and you will improve your skills
# and raise to new levels of magick.
#
# Now, you will practice some simple spells to get started on your journey to
# knowledge.
#
#
# RUNNING TESTS
# ==========
#
# To run tests for a level, use the test command, it is available 
#  in the bin directory.
# 
#     bin/test <level>
#
# Example: to run tests for level 1, this level
#
#  bin/test 1
#
# When you solved all the exercises and the tests pass, you need to send
# it for grading.
#
# Add a remote at the grading school:
#
#   git remote add grade ssh://spellbucks.patwic.com/<my-username>/functions.git
# 
#  and push the changes:
#
#   git push grade master:master
#
# The code you push will be tested and you will receive a mail with the results.
# If you pass all tests, you will ge a password to the next level.
#
#
# HINTS
# ====
#
# If you have trouble understanding how to do, you can get hints
# by decoding the hints belonging to a function.
#
# The hints are named HINTnnn, where nnn is number. To decode
# a hint, use the showHint command:
#
# bin/showHint HINT101
#
# will decode HINT100 and show it in the terminal.
#########

# Like any other profession, wizards needs to start modestly.
#
# Your apprentice name is "Bruglur...the somewhat comprehensible!"
def what_is_my_name():
    """Returns your name. Your name is "Bruglur...the somewhat comprehensible!".

    HINT111: VGhlIHJldHVybiBrZXl3b3JkIGZvbGxvd2VkIGJ5IHRoZSBuYW1l
    HINT112: TXVzdCBtYXRjaCBleGFjdGx5LCBkb3RzLCBzcGFjZXMsIGV4Y2xhbWF0aW9uIG1hcmsgYW5kIGFsbA==

    """
    name = "Bruglur...the somewhat comprehensible!"
    return name

# A wizard must know how to compose arcane chants.
def arcane_chant():
    """This function returns the arcane chant that means "Breath of the serpent,
    spell of life, the song for the maker".


    HINT121: SG93IGNhbiBhZGQgc3RyaW5ncyB0byBhIGxvbmdlciBzdHJpbmc/
    HINT122: VGhlIG9yZGVyIG9mIGNoYW50cyBpcyBnaXZlbiBpbiB0aGUgY29tbWVudC4=
    """
    spell_of_life = "orth bhais betha"
    the_song_for_the_maker = "do cheol deanta"
    breath_of_the_serpent = "anail nathrach"

    return breath_of_the_serpent + ", " + spell_of_life + ", " + the_song_for_the_maker

# Sometimes words have to be repeated.
def repetitive_chant():
    """Return the string "abra-abra-abra-abra-abra-abra-abra-kadabra" from a
    function which does not contain this string verbatim.

    Example: repetitive_chant() should return
             "abra-abra-abra-abra-abra-abra-abra-kadabra"

    HINT131: NSAiYWJyYS0icyBmb2xsb3dlZCBieSBhIGthZGFicmE=
    HINT132: V2hhdCBoYXBwZW5zIGluIHB5dGhvbiBpZiBhIHN0cmluZyBpcyBtdWx0aXBsaWVkIHdpdGggYW4gaW50ZWdlciBpbiBweXRob24/
    """
    return "abra-"*7 + "kadabra"
    
# To create "nothing" is not the same as not creating anything. Every wizard
# must know how to create an absence of anything.
def nothing():
    """Returns None.

    Just as 0 means "nothing" in matehematics, there is a concept of nothing in
    python programming. It's called:

    None

    HINT141: SnVzdCByZXR1cm4gTm9uZQ==

    """
    pass

