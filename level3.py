########
# Level 3: Artisan
#
# Using formulas with more than one component is a must for any aspiring
# wizard. You will also get the chance to weave two formulas together to make a
# third one from them. 
#
########

def triangle_area(base, height):
    """Compute the area of a triangle.

    Write code that computes the area from the base and height
    arugments and returns the area. The area is computed as
    (base * height) / 2.

    Example: triangleArea(2,4) should return 4

    Arguments:
    base     The triangle's base
    height   The triangle's height

    HINT311: WW91IHNob3VsZCByZXR1cm4gKGJhc2UgKiBoZWlnaHQpLyAyIGFzIHNob3duIGluIHRoZSBkZXNjcmlwdGlvbi4=
    """
    return (base * height) / 2

def percent(part, whole):
    """Given a part and a whole, calculate how many percent a the part is.

    Example: percent(1, 4) should return 25

    """
    return (part / whole) * 100

def max_of_two(a, b):
    """Given two numbers a and b, return the largest of them.

    The skeleton of the code is given below. Your task is
    to change the 0 in each branch, to a or b.

    Example: maxOfTwo(2, 4) should return 4

    Arguments:
    a       A number
    b       A number

    HINT331: R29vZCBqb2IsIHlvdW5nIG5lb3B5dGhlLiBZb3Ugbm93IGtub3cgaG93IHRvIGRlY29kZSBoaW50cyE=
    """
    if a > b:
        return a
    else:
        return b

def min_of_two(a, b):
    """Given two numbers a and b, return the smallest of them.

    Write the code to determine which of a and b is the smallest, and
    return that value.

    Example: maxOfTwo(2, 4) should return 2

    Arguments:
    a       A number
    b       A number

    HINT341: SXQgaXMgbGlrZSBtYXhfb2ZfdHdvLCBidXQgdGhlIG90aGVyIHdheSBhcm91bmQu
    """
    if a < b:
        return a
    else:
        return b
def max_of_four(a,b,c,d):
    """Compute the greatest of 4 numbers.

    Example: max_of_four(1, 13, 9, 4) should return 13

    Arguments a, b, c, and d are numbers.

    HINT351: VXNlIGEgc3BlbGwgeW91IGFscmVhZHkga25vdy4=
    HINT352: VXNlIG1heF9vZl90d28=
    HINT353: VXNlIG1heF9vZl90d28sIHRocmVlIHRpbWVzLg==
    """
    return max_of_two(max_of_two(a, b), max_of_two(c, d))
    

