########
# Level 4: Adept
#
# To understand the building blocks of magick you must delve deeper into the
# inner workings of combining spells.
#
########

def my_and(a,b):
    """Implement the and operator as a function.

    However, you can only use if-then statements and may not use the "and", "or" or "not" operators.

    Example: my_and(True, True) should return True.

    Arguments:
    a     a boolean argument
    b     a boolean argument

    HINT411: Ky0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSsKfCAgIGEgICB8ICAgIGIgICB8IFJlc3VsdGF0IHwKKy0tLS0tLS0rLS0tLS0tLS0rLS0tLS0tLS0tLSsKfCBGYWxzZSB8ICBGYWxzZSB8ICBGYWxzZSAgIHwKfCBGYWxzZSB8ICBUcnVlICB8ICBGYWxzZSAgIHwKfCBUcnVlICB8ICBGYWxzZSB8ICBGYWxzZSAgIHwKfCBUcnVlICB8ICBUcnVlICB8ICBUcnVlICAgIHwKKy0tLS0tLS0rLS0tLS0tLS0rLS0tLS0tLS0tLSs=
    """
    if a:
        return b
    else:
        return False

def my_or(a,b):
    """Implement the or operator as a function.

    However, you can only use if-then statements and may not use the "and", "or", or "not" operators.

    Example: my_or(False, True) should return True.

    Arguments:
    a     a boolean argument
    b     a boolean argument

    HINT421: Ky0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLSsKfCAgIGEgICB8ICAgIGIgICB8IFJlc3VsdGF0IHwKKy0tLS0tLS0rLS0tLS0tLS0rLS0tLS0tLS0tLSsKfCBGYWxzZSB8ICBGYWxzZSB8ICBGYWxzZSAgIHwKfCBGYWxzZSB8ICBUcnVlICB8ICBUcnVlICAgIHwKfCBUcnVlICB8ICBGYWxzZSB8ICBUcnVlICAgIHwKfCBUcnVlICB8ICBUcnVlICB8ICBUcnVlICAgIHwKKy0tLS0tLS0rLS0tLS0tLS0rLS0tLS0tLS0tLSs=
    """
    if a:
        return True
    else: 
        return b
    
def my_not(a):
    """Implement the not operator as a function.

    However, you can only use if-then statements and may not use the "and","or", or "not" operators.

    Example: my_not(False) should return True.
    Example: my_not(True) should return False.

    Argument a is a boolean.

    """
    if a: 
        return False
    else:
        return True

def my_xor(a, b):
    """Implement the xor operator as a function. The operator xor returns True
    if one and only one operand is True.

    Example: my_xor(False, False) should return False.
    Example: my_xor(False, True) should return True.
    Example: my_xor(True, True) should return False.

    Arguments a and b are boolean.

    """
    return a != b
        

# Occasionally, magical cubes need to be disarmed before they can be
# opened. Just like when summoning demons, this is done using a
# Graftglumpian Fizzle Circle. To save mana, the circle should be as
# small as possible.
#
# The library contains magic scrolls that you can use to create your spells. Use
# them as often as you can to reduce your own work.
def surrounding_circle(side):
    """Return the circumference of the smallest surrounding circle.

    If the side of a square is s, the distance between two
    non-adjacent (opposing) corners will be sqrt(2)*s (if you don't
    see this, just trust us). This will also be the diameter of the
    smallest circle surrounding the square (if you don't see this,
    draw them and think about it).

    Given the side of a square, surrounding_circle() should calculate
    the circumference of the smallest surrounding circle.

    """
    import math
    return math.sqrt(2)*side * math.pi

