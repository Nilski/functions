########
# Level 7: Archmage
#
# Congratulations on reaching this far. Many travellers fall aside by the road and
# never reach this level of insight!
#
# A few more obstacles remain, however, before you have reached full
# engligtenment. You need to display that even the most mindboggling
# spell do not cause you to waiver and falter. The questions in this level
# will test your knowledge of the nature of spells: how spells are created
# and how to determine what they are made of.
#
# Complete these last tasks and you have entered a select company: 
#  the Archmages!
# 
########

# To use another sorcerer's spell as part of one of your own spells gives you
# mighty powers.
def func_argument(f):
    """Call a function passed as argument to function.

    Functions are also objects in Python and can be passed as arguments just like
    strings and numbers. This function receives another function f as argument
    and your task is to invoke function f with the argument 1 and 2, and return
    what ever f returns.

    Argument:
    f           a function that takes two numbers as arguments and returns a value.

    HINT711: SXQgaXMgcmVhbGx5IHNpbXBsZSwganVzdCBkbyB3aGF0IHNlZW1zIG5hdHVyYWwu
    HINT712: SG93IGRvIHlvdSBjYWxsIGEgZnVuY3Rpb24/
    HINT713: cmV0dXJuIGYoMSwyKQ==
    """
    return f(1, 2)

# Being able to construct wondrous spells from textual descriptions is
# essential. Math can be considered another form of magick, but one
# that concerns itself with numbers alone. A basic spell requiring
# mastership of any aspiring mathemagician is quadratic equation solving.
# The formula you will write was first described by the French mathemagican
# René Descartes in 1637, so this is some seriously olde and well-proven magick.
#
# Quadratic equations are tricky beasts and may return different types
# of results:
#
#   1. There may be two distinct real roots
#   2. There may be exactly one real root
#   3. There may be two non-real roots, 2 complex numbers but these
#        are not for mortal eyes, so we will not concern ourselves with them
#        furher for now....
def second_degree(a,b,c):
    """Compute the roots of a second degree equation.

    The magic formula for computing second degree roots is

       ax^2 + bx + c = 0

    is

    Root 1: (-b - sqrt(b*b -4*a*c)) / 2*a
    Root 2: (-b + sqrt(b*b -4*a*c)) / 2*a

    You should return a string of the form "Root 1,Root 2"

    Higher level wizardy: if (b*b -4*a*c) < 0, then there no
    real solutions... the magic failed. In this case, return
    the string "No real solution,No real solution"

    Example: second_degree(2, 5, 2) should return "-2,-0.5"

    Arguments:
    a      A number, the quadratic coefficient
    b      A number, the linear coefficient
    c      A number, the constant term

    HINT721: VG8gY29udmVydCBhIG51bWJlciB0byBhIHN0cmluZyB1c2UgdGhlIGZ1bmN0aW9uIHN0cihudW1iZXIpLg==
    HINT722: Rmlyc3QgY29tcHV0ZSAoYipiLTQqYSpjKSwgeW91IHdpbGwgbmVlZCBpdCBtb3JlIHRoYW4gb25lIHRpbWUu
    HINT723: VG8gY29tcHV0ZSB0aGUgc3F1YXJlIHJvb3QsIGltcG9ydCBtYXRoIGFuZCB1c2UgbWF0aC5zcXJ0
    """
    from math import sqrt
    #import pdb
    #pdb.set_trace()
    skurt=(b*b -4*a*c)
    if skurt < 0:
        return "No real solution,No real solution"
    else:
        root1=(-b - sqrt(skurt) )/(2*a)
        root2=(-b + sqrt(skurt) )/(2*a)
        return str(float(root1)) + "," + str(float(root2))

# A real wizard must also be able to identify a spell cast at him/her to
# determine how to counter it.
def identifyLogicalFunction(func):
    """Determine which logical function was passed as argument.

    This function is passed one of the logical functions "and" and "or". It
    should determine which of the two it is.

    If it's neither "and" nor "or" it should return None.

    Example: identifyLogicalFunction(and_function) should return "and".
    Example: identifyLogicalFunction(xor_function) should return "xor".

    Arguments:
    func    A logical function with signature func(a, b). That is, func takes
            two boolean arguments a and b and return a boolean.

    HINT731: SG93IGRvIHlvdSBkZXRlcm1pbmUgd2hhdCBhIGZ1bmN0aW9uIHJldHVybnMsIGdpdmVuIGFuIGlucHV0Pw==
    HINT732: U2VlIEhJTlQ0MTEgYW5kIEhJTlQ0MjE=
    HINT733: Q2FsbCBmdW5jKGEsYiksIHdoZXJlIGEgYW5kIGIgYXJlIGRpZmZlcmVudCBjb21iaW5hdGlvbnMgb2YgVHJ1ZSBhbmQgRmFsc2UuIFRoZW4gY2hlY2sgdGhlIG91dHB1dC4=
    """
    
    och=func(True, True)
    exor=func(True, False) or (False, True)
    if exor==True and och==False:
        return "xor"
    
    elif och==True and exor==False:
        return "and"
        
    elif och==True and exor==True:
        return "or"
    elif och:
        return "and"
    else: 
        return None
    
    
    

# And now, for a final test, you will use some really dark magick.
# You need to cast a spell that can conjur new spell out of thin air,
# invoking some powerful forces. Beware, it is easy to go astray on your
# search here and conjur up a monster that cannot be controlled.
def higher_order_function(first_term):
    """A simple higher order function.

    A higher order function is a function that returns other functions.

    The task here is to return a function that takes an argument and then
    returns the sum of that argument *and* the argument first_term.

    Example:  if I first call:

            f = higher_order_function(10)
      then

           f(20)

       should return 30

    Arguments:

    first_term      A number with the first term be returned in the sum returned by the function
                         returned by this function.

    HINT741: QSBmdW5jdGlvbiBpcyBsaWtlIGFueSBvdGhlciBkYXRhIGluIHB5dGhvbi4=
    HINT742: RnVuY3Rpb25zIGNhbiBiZSBkZWNsYXJlZCBpbnNpZGUgZnVuY3Rpb25zLg==
    HINT743: RGVjbGFyZSBhIGZ1bmN0aW9uIGYgaW5zaWRlIHRoZSBmdW5jdGlvbiBoaWdoZXJfb3JkZXJfZnVuY3Rpb24sIGFuZCB0aGVuIHJldHVybiBmLg==
    """
    def f(ost):
        return ost + first_term
    return f
