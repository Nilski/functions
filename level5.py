########
# Level 5: Expert
#
# Chaining together spells makes it easy to create impressive effects. An
# expert wizard can do this in his sleep.
#
########

# Occasionally, a wizard needs to use one spell to conjure an object
# which will then be used as an ingredient for another spell.
def form(name):
    """Returns the string name.

    Example: form("ignem") should return "ignem".
    """
    return name

def technique(name, form):
    """Returns the string name with the form argument appended with a space in
    betweenf.

    Example: technique("creo", form("ignem")) should return "creo ignem".
    """
    return name + form

def pass_function_result_on():
    """Pass one function's return value as an argument to another function

    Pass the return value from a call to form() as an argument to a call to
    technique().

    The function should return the string "creo ignem".

    HINT511:  ZihnKCkp
    HINT512:  ZyBpcyBpbm5lciwgZiBpcyBvdXRlcg==
    """
    return technique("creo", form(" ignem"))

# A spellcaster who wishes to survive in the wilderness needs to know what to
# cast against whom.
def damage_done(foe, spell):
    """Calculate the number of hit points lost, 0-99.

    Arguments:
    foe      One of the strings "goblin", "giant", "snowman", "dragon"
    spell    One of the strings "fireball", "tornado", "freezebolt"

    HINT521: RHJhZ29uc2xpa2UgaGVhdCBidXQgc25vd21lbiBkb25cJ3Q=
    HINT522: R2lhbnRzIGRvbid0IGJsb3cgYXdheSBhcyBlYXNpbHkgYXMgZ29ibGlucyBkbw==

    """
    damage = {"goblin": {"fireball": 100, "tornado": 30, "freezebolt": 100},
              "giant": {"fireball": 55, "tornado": 5, "freezebolt": 40},
              "snowman": {"fireball": 90, "tornado": 30, "freezebolt": 2},
              "dragon": {"fireball": 3, "tornado": 40, "freezebolt": 80}}
    if foe == None or spell == None:
        return 0
    return damage[foe][spell]

# It might walk like a duck and swim like a duck and quack like a duck, but if
# it's called "Donald...the magnificent!", it's still a worthy adversary. The
# library contains scrolls with spells you can use.
def is_wizard(name):
    """Determine if a string is a magician's name by looking at whether
    it ends with "...the magnificent!"

    Use the python docs to find the solution at
    http://docs.python.org/3/library/stdtypes.html#str.endswith

    Example: is_wizard("Scrooge...the magnificent!") should return True

    Arguments:
    name      A string

    """
    if name.endswith( "...the magnificent!"):
        return True
    else:
        return False

