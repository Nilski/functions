########
# Level 6: Master
#
# Wheels within wheels within wheels... The recursive functions call themselves.
#
########

# Any wizard worth his salt must be able to determine how many potions he can
# create from his herbs. For instance, if I have fenugreek, wolfsbane, gurdyroot
# and fourfoil in my pouch, I could mix them in factorial(4) = 24 ways.
def recursive_factorial(n):
    """Compute the factorial of an integer.

    This can be used to compute how many ways to arrange n distinct objects into
    a sequence.

    The factorial function is defined as:

      factorial(0) = 1
      factorial(1) = 1
      factorial(n) = n * (n - 1) * (n - 2) * ... * 1 when n > 1

    Write the code for a recursive factorial function.

    Example: factorial(4) = 4 * 3 * 2 * 1 = 24

    HINT611: UmVjdXJzaW9uIGlzIHBvd2VyZnVsIG1hZ2ljay4=
    HINT612: Rmlyc3QgaGFuZGxlIHNwZWNpYWwgY2FzZXMgb2YgMCBhbmQgMS4gQSBzaW1wbGUgc3dpbmcgb2YgdGhlIHdhbmQgc2hvdWxkIHRha2UgY2FyZSBvZiB0aGVtLg==

    """
    if n <= 1:
        return 1
    else:
        return n * recursive_factorial(n-1)

# Many spells require the words be spoken backwards, so let's find out how we
# can do this.
def reverse_string(s):
    """Reverse a string.

    To do this, we need some additional capabilities. First, we need to
    get the first character in string, which is done by indexing the string.

    If a string has been defined:

      s = "A string"

    A string is sequence of characters, each of which can be accessed by its
    index.

    String:    'A string'
    Index:      01234567

    To get the first character of a string s, you would write:

      s[0]

    which returns

      'A'

    To get the second character:

      s[1]

    which returns

      ' '

    Now that we can get a single character, we need to get what is left of the string
    after the first character has been removed. This is achieved with a slicing operator,
    in our example:

      s[1:]

    This means, take all the characters from index 1 (remember that the index starts at 0)
    until the end of the string. It returns:

      ' string'

    Example: reverse_string('Avada Kedavra') should return 'arvadeK adavA'

    HINT621: VGhlIHJldmVyc2Ugb2YgIiIgaXMgIiIu
    HINT622: VG8gcmV2ZXJzZSBhIHN0cmluZywgcmVtb3ZlIHRoZSBmaXJzdCBjaGFyYWN0ZXIsIHRoZW4gcmV2ZXJzZSB0aGUgc3RyaW5nIHRoYXQgcmVtYWlucy4gUmV0dXJuIHRoYXQgKyB0aGUgZmlyc3QgY2hhcmFjdGVyLg==

    """
    if len(s) <= 1:
        return s
    else: 
        return reverse_string(s[1:]) + s[0]
        
